<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Initial page for the plug-in
 *
 * @package     local
 * @subpackage  feedback_ec10
 * @copyright   Eric Cheng ec10@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once '../../config.php';
require_once $CFG->dirroot.'/lib/formslib.php';
require_once $CFG->dirroot.'/lib/datalib.php';

class repeatable extends moodleform {
	
	function definition() {
		global $CFG;
		$mform = $this->_form;
		$repeatno = 1;
		
		$repeatarray = array();
		$repeatarray[] = $mform->createElement('text', 'student', get_string('student', 'local_feedback_ec10'));
		$repeatarray[] = $mform->createElement('hidden', 'studentnid', 0);

		$repeatarray2 = array();
		$repeatarray2[] = $mform->createElement('text', 'email', get_string('email', 'local_feedback_ec10'));
		$repeatarray2[] = $mform->createElement('hidden', 'emailid', 0);

		$repeateloptions = array();
		$repeateloptions['limit']['default'] = 0;
		
		//$repeatarray[] = $this->repeat_elements($repeatarray2, $repeatno, $repeateloptions, 'option_repeats1', 'option_add_fields2', 1, null, false);
		$this->repeat_elements($repeatarray, $repeatno, $repeateloptions, 'option_repeats', 'option_add_fields1', 1, null, true);
		$this->repeat_elements($repeatarray2, $repeatno, $repeateloptions, 'option_repeats', 'option_add_fields2', 1, null, true);
	}
}

class another_repeatable extends moodleform {
	function definition() {
		global $CFG;
		$mform = $this->_form;
		$repeatno = 1;

		$repeatarray2 = array();
		$repeatarray2[] = $mform->createElement('text', 'email', get_string('email', 'local_feedback_ec10'));
		$repeatarray2[] = $mform->createElement('hidden', 'emailid', 0);

		$repeateloptions = array();
		$repeateloptions['limit2']['default'] = 0;
		$this->repeat_elements($repeatarray2, $repeatno, $repeateloptions, 'Add email', 'option_add_fields2', 1, null, false);
	}
}

?>
