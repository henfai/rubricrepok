<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The form used by 'feedback_ec10'
 *
 * @package     local
 * @subpackage  feedback_ec10
 * @copyright   Eric Cheng ec10@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
//echo $OUTPUT->get_string('writing', 'local_feedback_ec10');
require_once '../../config.php';
require_once $CFG->dirroot.'/lib/formslib.php';
require_once $CFG->dirroot.'/lib/datalib.php';

/**
 * The search engine that allows the user to select the form to use for marking.
 */
class rubric_manager extends moodleform {
	function definition() {
		$mform = $this->_form;

		$mform->addElement('header', 'management_header', get_string('manage', 'local_feedback_ec10'));
		$mform->addElement('button', 'newform', get_string('createbutton', 'local_feedback_ec10'));
		//$mform->addElement('button', 'editform', get_string('editbutton', 'local_feedback_ec10'));
		$mform->addElement('button', 'delete+form', get_string('deletebutton', 'local_feedback_ec10'));
	}
}