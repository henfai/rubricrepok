<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the email template
 *
 * @package     local
 * @subpackage  feedback_kboyle
 * @copyright   Kieran Boyle kboyle@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 */


global $CFG, $PAGE, $DB;
 
require_once('../../config.php');

require_login();
require_capability('local/feedback_ec10:add', context_system::instance());
require_once($CFG->dirroot.'/local/feedback_ec10/preview_form.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('pluginname', 'local_feedback_ec10'));
$PAGE->set_heading(get_string('pluginname', 'local_feedback_ec10'));
$PAGE->set_url($CFG->wwwroot.'/local/feedback_ec10/preview.php?id='.$_GET['id']);
$preview_form = new create_preview_instance($CFG->wwwroot.'/local/feedback_ec10/preview.php?id='.$_GET['id']);//$CFG->wwwroot.'/local/feedback_ec10/creation.php?id='.$_GET['id']);

if ($preview_form->is_cancelled()) {
	/**
	 * TO-DO: Write a lib function that will delete all records for this form if user hits cancel.
	 */
	redirect($CFG->wwwroot.'/local/feedback_ec10/creation.php');
} elseif ($data = $preview_form->get_data()) {
	redirect($CFG->wwwroot.'/local/feedback_ec10/view.php');//need to post the value of the comments and the name of the feedbackform
} else {
	echo $OUTPUT->header();
	$preview_form->display();
	echo $OUTPUT->footer();
}

/*
	echo $OUTPUT->header();
	$preview_form->display();
	echo $OUTPUT->footer();
*/

?>