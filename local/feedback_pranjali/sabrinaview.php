<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Initial page for the plug-in
 *
 * @package     local
 * @subpackage  feedback_pranjali
 * @copyright   Eric Cheng ec10@ualberta.ca && Pranjali Pokharel pranjali@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

global $PAGE, $CFG;
require_once('../../config.php');

require_login();
require_capability('local/feedback_pranjali:add', context_system::instance());
require_once($CFG->dirroot.'/local/feedback_pranjali/repeat.php');

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout();
$PAGE->set_title(get_string('pluginname', 'local_feedback_pranjali'));
$PAGE->set_heading(get_string('pluginname', 'local_feedback_pranjali'));
$PAGE->set_url($CFG->wwwroot.'/local/feedback_pranjali/sabrinaview.php');
$form_search = new js_form();

/**
 * This block handles the logic for how to handle user submissions when selecting the course, discussion, and form.
 * 1. If all the search form is valid then POST all the information and re-direct to the corresponding form page.
 * 2. There is no form information, display the default screen.
 */
//if ($data = $form_search->get_data()) {
//	redirect($CFG->wwwroot.'/local/rubricrepo_sgannon1/feedback.php?formid='.$data->form);
//} else {
	echo $OUTPUT->header();
	$form_search->display();
	echo $OUTPUT->footer();
//}


?>