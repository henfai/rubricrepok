<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the email template
 *
 * @package     local
 * @subpackage  feedback_sgannon1
 * @copyright   Kieran Boyle sgannon1@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 */

require_once $CFG->dirroot.'/lib/formslib.php';
require_login();
/*
* This function creates and displays the email form
* It also fills out predefined feedback snippets for the user to enter
* this functionality will be further refined
*/
class create_email_instance extends moodleform{
	function definition(){
	global $CFG, $DB, $USER;
	$mform = $this ->_form;
	//set size of the header
    $attributes_heading = 'size="24"';
    $attributes_radio_text = 'size="11"';
     //$mfrom->addElement('text', 'emailSubject', get_string('emailSubjectHeader', 'local_rubricrepo_sgannon1'));
    $mform->addElement('textarea', 'subject', get_string('emailSubjectHeader', 'local_rubricrepo_sgannon1'), 'wrap="virtual" rows="1" cols="120" resize="none" style="resize:none"');
    //creates the text area for a user to enter in their information
    $mform->addElement('textarea', 'content', get_string('emailpreview', 'local_rubricrepo_sgannon1'), 'wrap="virtual" rows="30" cols="120" resize="none" style="resize:none"');
    //sets the content of the email
    //$emailBody = make_email();
    $formid = $DB->get_record('saved_form',array('id'=>$_GET['id']));
    $name = $DB-> get_record('user', array('id'=> $formid->studentid)); //need to get from the db
    $firstname = $name->firstname;
    //$firstcomment= $DB->get_record_sql('SELECT comment_text FROM {comments} WHERE id = ?',array(1));
    $body = '';
    $greeting = get_string('greeting', 'local_rubricrepo_sgannon1');


    $comma = get_string('comma', 'local_rubricrepo_sgannon1');
    $thanks = get_string('thanks', 'local_rubricrepo_sgannon1');
    $thread = $DB->get_record('saved_form',array('id'=>$_GET['id']));

    $forumpost = $DB->get_record('forum_posts',array('id'=>$thread->postid));

    $topic = $forumpost->subject; //need to get from db
    $goodStuff = get_string('goodStuff', 'local_rubricrepo_sgannon1');
    $body = $body. $greeting . $firstname . $comma . '


'. $thanks . ' '.$topic . '

'.$goodStuff;
    $savedCategories = $DB->get_records('saved_category',array('savedform_id'=>$_GET['id']));
    foreach ($savedCategories as $s) {
        echo $s->id;
        $categoryRow = $DB->get_record('category',array('id'=>$s->category_ref_id));
        $categoryName = $categoryRow->name;
        //echo $categoryName;
        //$body = $body . $categoryName;
        if($categoryRow->posneg == 0){
            $body = $body . '
        -' . $categoryName ;
        $commentRow = $DB->get_records('saved_comment',array('savedcategory_id'=>$s->id));
            foreach($commentRow as $c){
                $comment = $DB -> get_record('comments',array('id'=>$c->comment_ref_id));
                            $body = $body . '
                    -' . $comment->comment_text;

            }

        }


        //a$allComments
        # code...
    }

/*
    for($i=1;$i<11;$i++){
        $firstcomment = $DB->get_record('',array('id'=>$i));

    }
*/
     $badStuff = get_string('badStuff', 'local_rubricrepo_sgannon1');

     $body= $body .'

' .$badStuff;

    foreach ($savedCategories as $s) {
        echo $s->id;
        $categoryRow = $DB->get_record('category',array('id'=>$s->category_ref_id));
        $categoryName = $categoryRow->name;
        //echo $categoryName;
        //$body = $body . $categoryName;
        if($categoryRow->posneg == 1){
            $body = $body . '
        -' . $categoryName ;
        $commentRow = $DB->get_records('saved_comment',array('savedcategory_id'=>$s->id));
            foreach($commentRow as $c){
                $comment = $DB -> get_record('comments',array('id'=>$c->comment_ref_id));
                            $body = $body . '
                    -' . $comment->comment_text;

            }

        }


        //a$allComments
        # code...
    }
/*
     for($i=11;$i<15;$i++){
        $firstcomment = $DB->get_record('comments',array('id'=>$i));
        $body = $body . '
        -' . $firstcomment->comment_text;
    }
*/
    $finalWords =  get_string('finalWords', 'local_rubricrepo_sgannon1');
    $markerName = $USER->firstname; //from Sabrina Gannon
    $lastname = $USER->lastname;
    $body = $body . '

'. $finalWords .'




'. $markerName. ' ' . $lastname;





    $mform->setDefault('content',$body);
    //adds in the submit and cancel buttons.
    $this->add_action_buttons($cancel=true, $sumitlabel = get_string('sendEmail', 'local_rubricrepo_sgannon1'));//Courtsey of Henry Fok
    //$mform->addElement('submit', 'sendEmail', get_string('sendEmail', 'local_rubricrepo_sgannon1'));
	}
/*
    function make_email(){
        $body = 'Dear ';

        //for($i=1;$i<10;$i++){
            //$firstcomment=$DB->get_record('SELECT * FROM {mdl1_comment} WHERE id = ?',array(i));
            //body = $body . '' . $firstcomment;

        //}
        echo $body;
        //return $body;
    }
**/

};


?>
