<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Initial page for the plug-in
 *
 * @package     local
 * @subpackage  feedback_ec10
 * @copyright   Eric Cheng ec10@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

global $PAGE, $CFG, $DB;
require_once('../../config.php');

require_login();
require_capability('local/feedback_ec10:add', context_system::instance());
//require_once($CFG->dirroot.'/local/feedback_ec10/feedback_form.php');
require_once($CFG->dirroot.'/local/feedback_ec10/course_list.php');
//require_once($CFG->dirroot.'/local/feedback_ec10/forum_list.php');
require_once($CFG->dirroot.'/local/feedback_ec10/thread_search_engine.php');

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout();
$PAGE->set_title(get_string('pluginname', 'local_feedback_ec10'));
$PAGE->set_heading(get_string('pluginname', 'local_feedback_ec10'));
$PAGE->set_url($CFG->wwwroot.'/local/feedback_ec10/threadsearch.php?id='.$_GET['id']);

//$table1 = 'forum';
//$table2 = 'forum_discussions';
//$id = $_GET['id'];
//$course_search = new create_course_list();
	//} else {
	//	$PAGE->set_url($CFG->wwwroot.'/local/feedback_ec10/view.php?id='.$_GET['id'].'&fid='$_GET['fid']);
	//	if ($thread = $DB->get_records($table2, array('forum'=>$_GET['fid'], 'course'=>$_GET['id']))) {
	//		$arrgroup = array();
	//		foreach($thread as $t) {
	//			$arrgroup[$t->id] = $t->name;
	//		}
	//		$thread_search = new create_thread_list('', array('threads'=>$arrgroup));
	//	}
//echo 'Creating engine'.'<br>';
//$discussion_search = new create_discussion_list();
$forum_search_engine = new discussion_forum_search_engine($CFG->wwwroot.'/local/feedback_ec10/threadsearch.php?id='.$_GET['id']);
//$data = $discussion_search->get_data();
//echo 'discussion search created<br>';
//echo $data->forum;
//echo type($discussion_search);
//foreach ($discussion_search as $s) {
//	echo $s;
//}

//$form_search = new feedback_form_search();

/**
 * This block handles the logic for how to handle user submissions when selecting the course, discussion, and form.
 * 1. If all the search form is valid then POST all the information and re-direct to the corresponding form page.
 * 2. There is no form information, display the default screen.
 */
/*
switch ($_POST['search']) {
	case 'Search for forums':
		//$data = $course_search->get_data();
		redirect($CFG->wwwroot.'/local/feedback_ec10/view.php');
		break;
	case 'Search for threads':
	*/
//echo 'At the conditional'.'<br>';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		//foreach ($data as $d) {
		//	echo $d.'<br>';
		//	echo 'The forum is '.$d->forum.'<br>';
		//}
		$data = $forum_search_engine->get_data();
		redirect($CFG->wwwroot.'/local/feedback_ec10/view.php?id='.$data->forum_search_engine);
} else {
	//echo 'Building page'.'<br>';
	echo $OUTPUT->header();
		//$course_search->display();
	//echo $_GET['id'];
	$forum_search_engine->display();
	echo $OUTPUT->footer();
}
	//$form_search->display();
	//echo '<a href="/dev/ec10/feedback-plugin/local/feedback_ec10/js_test.php">JS Test</a>';
	//if($data) {
	//	echo $data;
	//}
	//echo $discussion_search;
	//echo $course_search->get_data();
	//echo $discussion_search->get_data();
	//$data = $discussion_search->get_data();
	//echo 'Better be data<br>';
	//echo $_GET['id'];
	


?>