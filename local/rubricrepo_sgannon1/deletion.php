<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the deletion template
 *
 * @package     local
 * @subpackage  feedback_sgannon1
 * @copyright   Kieran Boyle sgannon1@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 */


global $CFG, $PAGE, $DB;

require_once('../../config.php');

require_login();
require_capability('local/rubricrepo_sgannon1:add', context_system::instance());
require_once($CFG->dirroot.'/local/rubricrepo_sgannon1/deletion_form.php');

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('pluginname', 'local_rubricrepo_sgannon1'));
$PAGE->set_heading(get_string('pluginname', 'local_rubricrepo_sgannon1'));
$PAGE->set_url($CFG->wwwroot.'/local/rubricrepo_sgannon1/deletion.php');
$deletion_form = new create_deletion_instance();
$form_table = 'feedback_form';
$categories_table = 'category';
$comments_table = 'comments';


//echo $OUTPUT->header();
//$deletion_form->display();

/*
* This code is for loading the deletion page and displaying the contents
* and is responsible for  for redirecting and displaying the header and
* the footer
*
*/
$thatarray = array();
if ($deletion_form->is_cancelled()) {
	redirect($CFG->wwwroot.'/local/rubricrepo_sgannon1/view.php');
} elseif ($data = $deletion_form->get_data()) {
	$category_array = $DB->get_records($categories_table, array('form'=>$data->forms));
	foreach($category_array as $cat) {
		$comments= $DB->get_records($comments_table, array('category'=>$cat->id));
		foreach($comments as $value){
			$thatarray[] = $value->id;
		}

	}

	foreach($thatarray as $id){

		$DB->delete_records($comments_table, array('id'=>$id));
	}
	foreach ($category_array as $category) {
		$DB->delete_records($categories_table, array('id'=>$category->id));
	}
	$DB->delete_records($form_table, array('id'=>$data->forms));

	redirect($CFG->wwwroot.'/local/rubricrepo_sgannon1/view.php?');
} else {
	echo $OUTPUT->header();
	$deletion_form->display();
	echo $OUTPUT->footer();
}
?>
