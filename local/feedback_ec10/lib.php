<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of functions for the feedback plug-in
 *
 * @package     local
 * @subpackage  feedback_ec10
 * @copyright   Eric Cheng ec10@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


/**
 * This function extracts the value from the selected checkboxes ONLY.  The value returned for selected checkboxes
 * is the ID number of the corresponding comment for the feedback form.  If checkbox is not selected, the default
 * return value for the checkbox is 0, which is ignored.
 *
 * @return An array of comment objects.
 */
function get_comment_serials_from_form() {
	global $CFG, $DB;

	$grp_count = 0;
	$chkboxgrps = 'grp'.$grp_count;
	$table = 'comments';
	$comment_array = array();

	while (!empty($_POST[$chkboxgrps])) {
		//echo 'Checkboxes selected<br>';
		$checkbox = $_POST[$chkboxgrps];
		//echo $selected.'<br>';
		foreach($checkbox as $c) {
			
			// Ignore hidden elements and false advcheckbox elements
			if ($c != 0) {
				$result = $DB->get_record($table, array('id'=>$c));
				array_push($comment_array, $result);
				//echo $result->comment_text.'<br>';
			}
		}
		$grp_count++;
		$chkboxgrps = 'grp'.$grp_count;
	}
	return $comment_array;
}

/**
 * Saves the completed feedback form to the database.  Fields must not be empty when saving to prevent rows from being empty in the db.
 *
 * @return The ID of the saved form.
 */
function save_completed_form() {
	global $CFG, $DB, $USER;

	$table1 = 'saved_form';
	$table2 = 'saved_category';
	$table3 = 'saved_comment';
	$table4 = 'saved_freetxt';
	$table5 = 'forum_posts';

	$comments = get_comment_serials_from_form();
	$threadid = $_POST['threadid'];
	$thread_info = $DB->get_record($table5, array('id'=>$threadid)); // We need the thread info in order to get the student ID

	$new_save = new stdClass();
	$saved_freetxt = new stdClass();
	$saved_category = new stdClass();
	$saved_comment = new stdClass();
	
	$new_save->postid = $threadid;
	$new_save->instructorid = $USER->id;
	$new_save->studentid = $thread_info->userid;
	
	$savedform_id = $DB->insert_record($table1, $new_save); // By default, the ID of the new inserted record is returned.

	if($_POST['freetextcmt']) { // Save the comment in the free text box only if it is not empty.
		$saved_freetxt->savedform_id = $savedform_id;
		$saved_freetxt->comment_txt = $_POST['freetextcmt'];
		$DB->insert_record($table4, $saved_freetxt);
	}

	foreach($comments as $c) {
		$saved_category->savedform_id = $savedform_id;
		$saved_category->category_ref_id = $c->category;
		if(!$DB->get_record($table2, array('savedform_id'=>$savedform_id, 'category_ref_id'=>$c->category))) { //If the category for this saved form doesn't exist yet, 
																											   //then create the entry for it.
			$savedcategory_id = $DB->insert_record($table2, $saved_category);
		}
		$saved_comment->savedcategory_id = $savedcategory_id;
		$saved_comment->comment_ref_id = $c->id;
		$DB->insert_record($table3, $saved_comment);
	}

	return $savedform_id;
}
?>