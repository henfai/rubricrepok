<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Initial page for the plug-in
 *
 * @package     local
 * @subpackage  rubricrepo_sgannon1
 * @copyright   Eric Cheng ec10@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

global $PAGE, $CFG;
require_once('../../config.php');

require_login();
require_capability('local/rubricrepo_sgannon1:add', context_system::instance());
require_once($CFG->dirroot.'/local/rubricrepo_sgannon1/feedback_form.php');

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout();
$PAGE->set_title(get_string('pluginname', 'local_rubricrepo_sgannon1'));
$PAGE->set_heading(get_string('pluginname', 'local_rubricrepo_sgannon1'));
$PAGE->set_url($CFG->wwwroot.'/local/rubricrepo_sgannon1/view.php');
$form_search = new feedback_form_search();


$form_search->set_data($toform);

/**
 * This block handles the logic for how to handle user submissions when selecting the course, discussion, and form.
 * 1. If all the search form is valid then POST all the information and re-direct to the corresponding form page.
 * 2. There is no form information, display the default screen.
 */

if ($data = $form_search->get_data()) {
	//if(){
	redirect($CFG->wwwroot.'/local/rubricrepo_sgannon1/feedback.php?formid='.$data->form);
	//}else if ($form_search->no_submit_button_pressed()){
	//	redirect($CFG->wwwroot.'/local/rubricrepo_sgannon1/creation.php?');
	}else {
	echo $OUTPUT->header();
	//<FORM>
	//<INPUT TYPE="button" onClick="parent.location='/local/rubricrepo_sgannon1/creation.php'">
	//</FORM>
	$form_search->display();
	echo $OUTPUT->footer();
}



?>
