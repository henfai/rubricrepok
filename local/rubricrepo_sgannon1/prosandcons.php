<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the email template
 *
 * @package     local
 * @subpackage  feedback_sgannon1
 * @copyright   Kieran Boyle sgannon1@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 */


global $CFG, $PAGE, $DB;

require_once('../../config.php');

require_login();
require_capability('local/rubricrepo_sgannon1:add', context_system::instance());
require_once($CFG->dirroot.'/local/rubricrepo_sgannon1/prosandcons_form.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('pluginname', 'local_rubricrepo_sgannon1'));
$PAGE->set_heading(get_string('pluginname', 'local_rubricrepo_sgannon1'));
$PAGE->set_url($CFG->wwwroot.'/local/rubricrepo_sgannon1/prosandcons.php?id='.$_GET['id']);
$maker_form = new create_addcomments_instance($CFG->wwwroot.'/local/rubricrepo_sgannon1/prosandcons.php?id='.$_GET['id']);//$CFG->wwwroot.'/local/rubricrepo_sgannon1/creation.php?id='.$_GET['id']);
$commentsTable = 'comments';
//echo $OUTPUT->header();
//$email_form->display();

/*
* This code is for loading the email page and displaying the contents
* and is responsible for  for redirecting and displaying the header and
* the footer
*
*/

if ($maker_form->is_cancelled()) {
	//redirect($CFG->wwwroot.'/local/rubricrepo_sgannon1/creation.php');
} elseif ($data = $maker_form->get_data()) {
	//$check = $data->test1;
	$comments = $data->comment;
	$categoryAssociations = $data->categorySelect;
	$i = 0;
	foreach ($comments as $comment) {
		$categoryAssociation = $categoryAssociations[$i];
		$commentRecord = new stdClass();
		$commentRecord->comment_text = $comment ;
		$commentRecord->category = $categoryAssociation;
		$DB->insert_record($commentsTable, $commentRecord ,$returnid=false, $bulk=false);
		$i = $i + 1;
	}

	redirect($CFG->wwwroot.'/local/rubricrepo_sgannon1/preview.php?id='.$_GET['id']);//need to post the value of the comments and the name of the feedbackform
} else {
	echo $OUTPUT->header();
	$maker_form->display();
	echo $OUTPUT->footer();
}

/*
	echo $OUTPUT->header();
	$maker_form->display();
	echo $OUTPUT->footer();
*/
?>
