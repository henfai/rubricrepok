
<?php

/*
* This code is used in the creation of forms (and can be used in the future).
* Used to dynamically add fields to a form object.
* Code written by Sabrina, method found by Sabrina 
*/
require_once '../../config.php';
require_once $CFG->dirroot.'/lib/formslib.php';
require_once $CFG->dirroot.'/lib/datalib.php';

class js_form extends moodleform {

    function definition() {
        global $CFG, $DB; // These are required to be declared inside the constructor.
        $mform = $this->_form;

        $arrgroup = array('1'=>'SomeString');

        $mform->addElement('select', 'parent', 'This is an option', $arrgroup);
        $mform->addElement('static', 'child', null, 'text');
        $other = array();
        $other[] =  $mform->createElement('text', 'option', 'hi');
        $other[] = $mform->createElement('text', 'limit', 'hi2');
        $other[] = $mform->createElement('hidden', 'optionid', 0);
        $repeatarray = array(); 
        $repreatarray[] =  $mform->createElement('text', 'option', 'hi');
        $repeatarray[] = $mform->createElement('text', 'limit', 'hi2');
        $repeatarray[] = $mform->createElement('hidden', 'optionid', 0);
 
        $repeateloptions = array();
        $repeateloptions['limit']['default'] = 0;
        $repeateloptions['limit']['disabledif'] = array('limitanswers', 'eq', 0);
        $repeateloptions['limit']['rule'] = 'numeric';
        $repeateloptions['limit']['type'] = PARAM_INT;
 
        $repeateloptions['option']['helpbutton'] = array('choiceoptions', 'choice');
        $mform->setType('option', PARAM_CLEANHTML);
 
        $mform->setType('optionid', PARAM_INT);
        $this->repeat_elements($other, $repeatno, $repeateloptions, 'option_repeats', 'option_add_fields', 1, null, true);
        $this->repeat_elements($repeatarray, $repeatno, $repeateloptions, 'option_repeats', 'option_add_fields', 1, null, true);
                    }
                }

?>