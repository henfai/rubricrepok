<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the email template
 *
 * @package     local
 * @subpackage  feedback_sgannon1
 * @copyright   Kieran Boyle sgannon1@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 */

require_once $CFG->dirroot.'/lib/formslib.php';
require_login();
/*
* This function creates and displays the email form
* It also fills out predefined feedback snippets for the user to enter
* this functionality will be further refined
*/
class create_makeForm_instance extends moodleform{
	function definition(){
	   global $CFG, $DB, $USER;
       //$this->page->requires->js_init_call('M.local_rubricrepo_sgannon1.init',array('this is the param1 value'), false, $jsmodule);
         $mform = $this ->_form;
         $procon = array(0=>'Positive',1=>'Negative');
       //$this->page->requires->js_init_call('M.local_rubricrepo_sgannon1.init',array('this is the param1 value'), false, $jsmodule);
       //$mfrom->addElement('text', 'formName', get_string('formName', 'local_rubricrepo_sgannon1'));
         $mform->addElement('header','formdescription',get_string('formInfo', 'local_rubricrepo_sgannon1'));
         $mform->addElement('text', 'formName', get_string('student', 'local_rubricrepo_sgannon1'));
         $radioarray= array();
         $radioarray[] = $mform->createElement('radio','visibility','',get_string('isItpub','local_rubricrepo_sgannon1'),0);
         $radioarray[] = $mform->createElement('radio','visibility','',get_string('isItpriv','local_rubricrepo_sgannon1'),1);
         $mform->addGroup($radioarray,'radiogroup','Accessibility', array('   '), false);
         $mform->setDefault('visibility', 0);

         $mform->addElement('header','description',get_string('addCategoryDesc', 'local_rubricrepo_sgannon1'));
       //$mform->addElement('header', 'categories',get_string('numCategories', 'local_rubricrepo_sgannon1'));
       //$mform->addElement('textarea', 'numCategories', get_string('numCategories', 'local_rubricrepo_sgannon1'), 'wrap="virtual" rows="10" cols="60" resize="none" style="resize:none"');
       $repeatarray = array();
       $repeatarray[] = $mform->createElement('text', 'category', get_string('student', 'local_rubricrepo_sgannon1'));
       $repeatarray[] = $mform->createElement('select','proscons', get_string('classification', 'local_rubricrepo_sgannon1'), $procon);
       ///$repeatarray[] = $mform->createElement('button','addComment', 'Add comment');
       ///$repeatarray[] = $mform->createElement('text', 'option', get_string('optionno', 'choice'));
       $repeatableoptions = array();
       $repeateloptions['category']['default'] = '';
       //$mform->setType('option', PARAM_CLEANHTML);
       //$mform->setType('optionid', PARAM_INT);
       $repeatno = 1;

       $this->repeat_elements($repeatarray, $repeatno, $repeateloptions, 'option_repeats', 'option_add_fields', 1, get_string('numCategories', 'local_rubricrepo_sgannon1'), false);
       $this->add_action_buttons($cancel=true, $sumitlabel = get_string('nextPage', 'local_rubricrepo_sgannon1'));

    }

};


?>
