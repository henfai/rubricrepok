<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the email template
 *
 * @package     local
 * @subpackage  feedback_ec10
 * @copyright   Kieran Boyle kboyle@ualberta.ca && Eric Cheng ec10@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 */

require_once $CFG->dirroot.'/lib/formslib.php';
require_login();
/*
* This function creates and displays the email form
* It also fills out predefined feedback snippets for the user to enter
* this functionality will be further refined
*/
class create_email_instance extends moodleform{
	function definition(){
	global $CFG, $DB, $USER;
	$mform = $this ->_form;
	//set size of the header
    $attributes_heading = 'size="24"';
    $attributes_radio_text = 'size="11"';
     //$mfrom->addElement('text', 'emailSubject', get_string('emailSubjectHeader', 'local_feedback_ec10'));
    //$mform->addElement('textarea', 'subject', get_string('emailSubjectHeader', 'local_feedback_ec10'), 'wrap="virtual" rows="1" cols="120" resize="none" style="resize:none"');
    //creates the text area for a user to enter in their information
    //$mform->addElement('editor', 'content', '');
    //$mform->setType('content', PARAM_RAW);
    //sets the content of the email
    //$emailBody = make_email();
    $formid = $DB->get_record('saved_form',array('id'=>$_GET['id']));
    $name = $DB-> get_record('user', array('id'=> $formid->studentid)); //need to get from the db
    $firstname = $name->firstname;
    //$firstcomment= $DB->get_record_sql('SELECT comment_text FROM {comments} WHERE id = ?',array(1));
    $greeting = get_string('greeting', 'local_feedback_ec10');
    $thanks = get_string('thanks', 'local_feedback_ec10');

    $thread = $DB->get_record('saved_form',array('id'=>$_GET['id']));
    $forumpost = $DB->get_record('forum_posts',array('id'=>$thread->postid)); 

    $topic = $forumpost->subject; //need to get from db
    $goodStuff = get_string('goodStuff', 'local_feedback_ec10');
    $body = '<p>'.$greeting.' '.$firstname.','.'</p>'.'<p>'.$thanks.$topic.'.'.'</p>';

    if ($DB->get_records('saved_freetxt', array('savedform_id'=>$_GET['id']))) {
        $freetxt = $DB->get_record('saved_freetxt', array('savedform_id'=>$_GET['id']));
        $body = $body.'<p>'.$freetxt->comment_txt.'</p>';
    }

    $body = $body.'<p>'.$goodStuff.'</p>';
    $savedCategories = $DB->get_records('saved_category',array('savedform_id'=>$_GET['id']));
    foreach ($savedCategories as $s) {
        //echo $s->id;
        $categoryRow = $DB->get_record('category',array('id'=>$s->category_ref_id));
        $categoryName = $categoryRow->name;
        //echo $categoryName;
        //$body = $body . $categoryName;
        if($categoryRow->posneg == 0){
            $body = $body.'<ul>'.'<li>'.$categoryName.'<ul>';
            $commentRow = $DB->get_records('saved_comment',array('savedcategory_id'=>$s->id));

            foreach($commentRow as $c){
                $comment = $DB -> get_record('comments',array('id'=>$c->comment_ref_id));
                $body = $body.'<li>'.$comment->comment_text.'</li>';
            }
            $body = $body.'</ul>'.'</ul>'.'</li>';
        }
    }
    $body = $body.'</ul>';
/*
    for($i=1;$i<11;$i++){
        $firstcomment = $DB->get_record('',array('id'=>$i));

    }
*/
    $badStuff = get_string('badStuff', 'local_feedback_ec10');
    $body= $body.'<p>'.$badStuff.'</p>';

    foreach ($savedCategories as $s) {
        //echo $s->id;
        $categoryRow = $DB->get_record('category',array('id'=>$s->category_ref_id));
        $categoryName = $categoryRow->name;
        //echo $categoryName;
        //$body = $body . $categoryName;
        if($categoryRow->posneg == 1){
            $body = $body.'<ul>'.'<li>'.$categoryName.'<ul>';
            $commentRow = $DB->get_records('saved_comment',array('savedcategory_id'=>$s->id));
            
            foreach($commentRow as $c){
                $comment = $DB -> get_record('comments',array('id'=>$c->comment_ref_id));
                $body = $body.'<li>'.$comment->comment_text.'</li>';
            }
            $body = $body.'</ul>'.'</ul>'.'</li>';
        }
    }
    $body = $body.'</ul>';
/*
     for($i=11;$i<15;$i++){
        $firstcomment = $DB->get_record('comments',array('id'=>$i));
        $body = $body . ' 
        -' . $firstcomment->comment_text;
    }
*/
    $finalWords = get_string('finalWords', 'local_feedback_ec10');
    $markerName = $USER->firstname; //from Sabrina Gannon
    $lastname = $USER->lastname;
    $body = $body .'<p>'.$finalWords .'</p>'.'<p>'.$markerName.' '.$lastname.'</p>';

    //$mform->setDefault('content[text]',$body);
    $mform->addElement('editor', 'content', null)->setValue(array('text'=>$body));
    $mform->setType('content', PARAM_RAW);
    //adds in the submit and cancel buttons.
    $this->add_action_buttons($cancel=true, $submitlabel = get_string('sendEmail', 'local_feedback_ec10'));//Courtsey of Henry Fok
    //$mform->addElement('submit', 'sendEmail', get_string('sendEmail', 'local_feedback_ec10'));
	}
/*
    function make_email(){
        $body = 'Dear ';
    
        //for($i=1;$i<10;$i++){
            //$firstcomment=$DB->get_record('SELECT * FROM {mdl1_comment} WHERE id = ?',array(i));
            //body = $body . '' . $firstcomment;
            
        //}
        echo $body;
        //return $body;
    }
**/

};


?>