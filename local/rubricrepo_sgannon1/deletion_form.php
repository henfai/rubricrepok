<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the email template
 *
 * @package     local
 * @subpackage  feedback_sgannon1
 * @copyright   Kieran Boyle sgannon1@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 */

require_once $CFG->dirroot.'/lib/formslib.php';
require_login();
/*
* This function creates and displays the email form
* It also fills out predefined feedback snippets for the user to enter
* this functionality will be further refined
*/
class create_deletion_instance extends moodleform{
	function definition(){
	   global $CFG, $DB, $USER;
   		 $forms = array();
			 $userid = $USER->id;
       $mform = $this ->_form;
			 $formtable = 'feedback_form';
			 $form_ids = $DB->get_records($formtable, array('userid'=>$userid));
			 foreach($form_ids as $f) {
				 $forms[$f->id] = $f->title;
			 }
       $mform->addElement('select','forms', get_string('form2Delete', 'local_rubricrepo_sgannon1'), $forms);
       $this->add_action_buttons($cancel=true, $submitlabel = get_string('nextPage', 'local_rubricrepo_sgannon1'));

    }

};


?>
