<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the email template
 *
 * @package     local
 * @subpackage  feedback_kboyle
 * @copyright   Kieran Boyle kboyle@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 */

require_once $CFG->dirroot.'/lib/formslib.php';
require_login();

class create_newComments_instance extends moodleform{
    function definition(){
        global $CFG, $DB, $USER;
        $mform = $this ->_form;
        $formtable = 'feedback_form';
        $categorytable = 'category';
        $repeatno = 1;

        $form = $DB->get_record($formtable,array('id'=>$_GET['id']));
        $formname = $form->title;
        $categoryarray= array();
        $categories = $DB->get_records($categorytable,array('form'=>$_GET['id']));
        foreach ($categories as $cat) {
            if($cat->posneg == 0){
                $categoryarray[$cat->id] = $cat->name.' (Positive)';
            }else{
                $categoryarray[$cat->id] = $cat->name.' (Negative)';

            }
        }

        $mform->addElement('header','addCommentsHeader',get_string('addCommentsHeader', 'local_rubricrepo_kboyle'));
        $repeatarray2 = array();
        //$mform->addElement('header','definedCategory',$formname);
        $repeatarray2[] = $mform->createElement('text', 'comment', get_string('commenting', 'local_rubricrepo_kboyle'));
        $repeatarray2[] = $mform->createElement('select','categorySelect', get_string('association', 'local_rubricrepo_kboyle'), $categoryarray);
        $repeatableoptions = array();
        $repeateloptions['category']['default'] = '';

        $this->repeat_elements($repeatarray2, $repeatno, $repeateloptions, 'option_repeats2', 'option_add_fields', 1, get_string('addComment', 'local_rubricrepo_kboyle'), false);
        $mform->addElement('submit', 'addCommentsButton', get_string('addInfoToForm', 'local_rubricrepo_kboyle'));
        //$this->add_action_buttons($cancel=true, $sumitlabel = get_string('saveIt', 'local_rubricrepo_kboyle'));
    }

};

?>