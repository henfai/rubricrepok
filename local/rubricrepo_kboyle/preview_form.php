<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the email template
 *
 * @package     local
 * @subpackage  feedback_kboyle
 * @copyright   Kieran Boyle kboyle@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 */
require_once '../../config.php';
require_once $CFG->dirroot.'/lib/formslib.php';
require_once $CFG->dirroot.'/lib/datalib.php';
require_login();
/*
* This function creates and displays the email form
* It also fills out predefined feedback snippets for the user to enter
* this functionality will be further refined
*/
class create_preview_instance extends moodleform{
	function definition(){
		global $CFG, $DB;
    	$mform = $this->_form;

		$table1 = 'category';
		$table2 = 'comments';
		$cat_count = 0;
		$commgrp_count = 0;
		$formid = $_GET['id'];

		if ($result = $DB->get_records($table1, array('form'=>$formid))) {

			//$mform->addElement('text', 'subject', get_string('subject', 'local_rubricrepo_kboyle'), $attributes);
			//$mform->addElement('text', 'student_name', get_string('student', 'local_rubricrepo_kboyle'), $attributes);
			//$mform->addElement('text', 'student_email', get_string('email', 'local_rubricrepo_kboyle'), $attributes);
			//$mform->addElement('textarea', 'intro', get_string('freetext', 'local_rubricrepo_kboyle'), 'wrap="virtual" rows="10" cols="60" resize="none" style="resize:none"');
			foreach($result as $r) {
				//echo $r->name.' '.$r->id.'<br>';
				$header = 'header'.$cat_count;
				$mform->addElement('header', $header, $r->name);
				$mform->setExpanded($header, true);
				$comment = $DB->get_records($table2, array('category'=>$r->id));
				$cat_count++;
				$i = 0;
				foreach ($comment as $c) {
					//$chkboxgrp = 'grp'.$commgrp_count.'[]';
					//$mform->addElement('advcheckbox', $chkboxgrp, null, $c->comment_text, array('group'=>$commgrp_count), array(0,$c->id));
					$mform->addElement('static','comment'.$i, get_string('empty','local_rubricrepo_kboyle'),$c->comment_text);
					$i = $i+1;
					//echo $c->comment_text.' '.$c->id.'<br>';
				}
				//$this->add_checkbox_controller($commgrp_count);
				//$commgrp_count++;
			}
			$this->add_action_buttons();
		} 
	}
};


?>