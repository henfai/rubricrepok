<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The form used by 'feedback_ec10'
 *
 * @package     local
 * @subpackage  feedback_ec10
 * @copyright   Eric Cheng ec10@ualberta.ca && Pranjali Pokharel pranjali@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
//echo $OUTPUT->get_string('writing', 'local_feedback_ec10');
require_once '../../config.php';
require_once $CFG->dirroot.'/lib/formslib.php';
require_once $CFG->dirroot.'/lib/datalib.php';


/**
 * The search engine that allows the user to select the form to use for marking.
 */
class create_discussion_list extends moodleform {
	function definition() {
		global $CFG, $DB;
		$mform = $this->_form;
		$table1 = 'forum';
		//$id = 0;
		$id = $_GET['cid'];
		//$id = 2;
		//echo $id.'is of type '.gettype($id).'<br>';
		$arrgroup = array();
		//$arrgroup[0] = '';
		
		$discussion = $DB->get_records($table1, array('course'=>$id, 'type'=>'general'));

		foreach($discussion as $d) {
			//echo $d->id;
			//echo $d->name;
			//echo $d;
			//$num = $d->id;
			//$name = $d->name;
			//echo gettype($d->name).'<br>';
			$arrgroup[$d->id] = $d->name;
		}
		
		//foreach($arrgroup as $k=>$d) {
			//echo $k;
			//echo $d;
			//$select_forum_array[$k] = $d;
		//}
		
		$selectgroup = array();
		$selectgroup[] = $mform->createElement('header', 'forumheader', get_string('useforum', 'local_feedback_ec10'));
		$selectgroup[] = $mform->createElement('select', 'forum', get_string('forumlist', 'local_feedback_ec10'), $arrgroup);
		//foreach($arrgroup as $a) {
		//	$selectgroup[] = $mform->createElement('radio', 'yesno', '', $a, 0, null);
		//}
		//$mform->addGroup()
		$selectgroup[] = $mform->createElement('submit', 'search', get_string('forumsearch', 'local_feedback_ec10'));
		$mform->addGroup($selectgroup, 'selectforum', get_string('selectforum', 'local_feedback_ec10'), '  ', false);
		//$this->add_action_buttons();
		//$mform->addElement('select', 'forum', get_string('forumlist', 'local_feedback_ec10'), $arrgroup);
		//$mform->addElement('submit', 'search', get_string('forumsearch', 'local_feedback_ec10'));
	}
}

?>